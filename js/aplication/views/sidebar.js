//sidebar
module.exports = function(bootstrap, React, Router) {
	return React.createClass({
  	render: function() {
		
		var Dashboard = require('./dashboard.js');
		var DashboardView = Dashboard(bootstrap, React);
	
    	return (	  
      	<div className="sidebar">
	  		<h2>Navigation</h2>
       		  <ul>
				<li><Link to="Dashboard">Dashboard</Link></li>
            	<li><Link to="team">Inbox</Link></li>           
          	</ul>	
      	</div>
	  
    	);
  	}
	});
}

var routes = (
  	<Route name="Mainsection" path="/" handler={Mainsection}>
    <Route name="team" handler={team}/>
    <Route name="Dashboard" handler={Dashboard}/>
	<Route name="project_detail" handler={project_detail}/>
    <DefaultRoute handler={Dashboard}/>
  </Route>
);

ReactRouter.run(routes, function (Handler) {
  React.render(<Handler/>, mountNode );
});

