//wrap all green header
module.exports = function(bootstrap, React, domLocation) {
var ModalTrigger = bootstrap.ModalTrigger;
var Button = bootstrap.Button;
var Modal = bootstrap.Modal;
var OverlayMixin = bootstrap.OverlayMixin;
var Grid = bootstrap.Grid;
var Row = bootstrap.Row;
var Col = bootstrap.Col;
	
var GreenHeader = React.createClass({
  render: function() {
    return (
      <div className="greenheader">
       <Container />
      </div>
    );
  }
});



//container header
var Container = React.createClass({
  render: function() {
    return (
      <div className="container-fluid add-padding">
			<Row>
				<Col lg={5}>
					
	  				<Logo /> 
				</Col>
				<Col lg={2} className="centering">
					
	  				<CustomModalTrigger />
				</Col>
				<Col lg={5}>
					
	  				<UserImage />
				</Col>
			</Row>
      </div>
    );
  }
});




//logo
var Logo = React.createClass({
  render: function() {
    return (
      <div className="logo-name">
	  	<div className="logo-top">
	  		<a href="">Time tracker</a>
      	</div>
      </div>
    );
  }
});


// modal add task on header (click to add task)
var CustomModalTrigger = React.createClass({
	
	  	
  mixins: [OverlayMixin],

  getInitialState: function () {
    return {
      isModalOpen: false
    };
  },

  handleToggle: function () {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  },

  render: function () {
    return (
      <Button onClick={this.handleToggle} bsStyle="primary">&#43;</Button>
    );
  },

 
  renderOverlay: function () {
    if (!this.state.isModalOpen) {
      return <span/>;
    }

    return (
        <Modal  onRequestHide={this.handleToggle}>
		  <div className="arrow-tooltip"></div>	
		  <div className="head-project-wraper">
                <h2 className="title-container-project">Name your project..</h2>
          </div> 
          <div className="modal-body">
             <textarea className="project-input" placeholder="Type description.."></textarea>
          </div>
          <div className="modal-footer">
           	 <input type="text" className="input-date datetimepicker" placeholder="Start time.." />
             <input type="text" className="input-date datetimepicker" placeholder="End time.." />      
             <input type="submit" value="create" className="create-button" />
          </div>
        </Modal>
      );
  }
});



//user image (ini ane belum faham cara ambil data image ak asep maknya statis dulu heheheh)
var UserImage = React.createClass({
  render: function() {
    return (
      <div className="img-user">
	  		<img src={'img/user-profile.jpg'} /> 
			
      </div>
    );
  }
});

React.render(<GreenHeader />, domLocation);

}
