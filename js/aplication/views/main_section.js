// main component============================
module.exports = function(bootstrap, React, Router, domLocation) {	
var Row = bootstrap.Row;
var Col = bootstrap.Col;
var Dashboard = require('./dashboard.js');
var DashboardView = Dashboard(bootstrap, React);
	
var Mainsection = React.createClass({
  render: function() {
		
	var Sidebar = require('./sidebar.js');
	var SidebarView = Sidebar(bootstrap, React);
	
		
		return (
      <div className="main-section">
       	 <div className="container-fluid no-padding">
			<Row>
				<Col lg={2} className="no-padding">
					<SidebarView />					
				</Col>
				<Col lg={10} className="no-padding Grey">
					<div className="right-section">
						<RouteHandler/>
					</div>
				</Col>
			</Row>
      </div>
      </div>
    );
  }
});

React.render(<Mainsection />, domLocation);


}

